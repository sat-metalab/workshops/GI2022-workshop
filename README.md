# GI2022 Workshop

Workshop plan and materials for GI2022 conference

Creating Spontaneous and Mobile Immersive Spaces using FLOSS

https://graphicsinterface.org/conference/2022/program/workshop/

## Authors

* [Christian Frisson](https://frisson.re)
* Emmanuel Durand
* Michał Seta

## License

All of this work is under the [CC-BY-SA 4.0 licence](https://creativecommons.org/licenses/by-sa/4.0/), except for the Blender scene used for the demo which from [The Mantissa](https://mantissa.xyz/pages/vj.html) under CC0 (Public Domain).

## Presentation

Source: [slides.md](slides.md)

### Requirements

- Install Node.js and NPM.
- Install dependencies (once or after upgrades):
```
npm i
```

### Development

Write markdown while [reveal-md](https://github.com/webpro/reveal-md)'s local server converts to html slides.

- Run the local server:
```
npm run dev
```

### Release

- Convert md to html:
```
npm run html
```
- (Optional) Convert md to pdf:
```
npm run pdf
```
- Publish to github pages (gitlab pages automated with [continuous integration](.gitlab-ci.yml))
```
npm run publish
```

### Controls

- Use `Ctrl + click` to zoom on slides, useful for images
- Press `o` to show the slide overview
- Press `s` to open the speaker notes window (requires allowing popups)

Check the [reveal.js](https://github.com/hakimel/reveal.js/#speaker-notes) README for more tips.
