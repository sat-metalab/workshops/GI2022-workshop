GI'2022 workshop demo
=====================

## How to run

To run Splash:
```bash
cd splash
splash config.json
```

Then calibrate the video-projectors.

To run LivePose (after having installed it):
```bash
livepose livepose/config.json
```

You might have to run LivePose from its source directory, in which case you should update the path.
