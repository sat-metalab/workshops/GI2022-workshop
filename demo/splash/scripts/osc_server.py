import liblo
import re

from time import sleep
from typing import Any


class OSCServer():
    def __init__(self, host, port=9000):
        self.port = port
        self.server = liblo.Server(port)
        self.path_specs = []
        self.registerCallbacks()
        self.debug = False
        self.host = host

        self.eye_position = [0.0, 0.0, 0.0]

    def stop(self):
        self.server.free()
        del self.server

    def getPort(self):
        return self.port

    def recv(self, timeout):
        try:
            self.server.recv(timeout)
        except Exception as e:
            print("cannot receive because ", e)

    def registerCallbacks(self):
        self.server.add_method(None, None, self.dispatch_message)
        self.add_regex_method("/livepose/pyrealsense_eyes_follower/0/(?P<n>.+)", "s", self.handle_object)

    def add_regex_method(self, spec, type, func):
        self.path_specs.append((spec, type, func))

    def dispatch_message(self, path, args, types, src):
        if self.debug:
            print("received OSC message ", path, args, types, src)
        matched = False
        for spec, t, f in self.path_specs:
            matches = re.match(spec, path)
            if matches is not None:
                d = matches.group()
                if self.debug:
                    print("matched group ", d)
                f(path, args)
                matched = True
        if not matched:
            if self.debug:
                print("not matched: ", path)
            pass

    def handle_object(self, path, args):
        if self.debug:
            print("message for {} with following arguments: {}".format(path, args))

        if len(args) != 3:
            return

        if args[0] == 0.0 and args[1] == 0.0 and args[2] == 0.0:
            return

        follower_id = int(self.handle_path(path))

        if follower_id == 0:
            self.eye_position = [-args[0], args[1], -args[2]]

    def handle_path(self, p):
        """split the path and return the last element"""
        return p.split("/")[-1]
