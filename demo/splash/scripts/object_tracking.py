#!/usr/bin/env python3
# This script controls a Splash object from a VRPN tracker
import argparse
import code
import sys
import threading

import splash  # type: ignore

from osc_server import OSCServer

repl_active = False
hostname = 'localhost'
oscserver = None
run_loop = True
eye_position = [0.0, 0.0, 0.0]


try:
    import readline
except ImportError:
    pass
else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")
    readline.set_completer(rlcompleter.Completer(globals()).complete)


def repl():
    global console
    console = code.InteractiveConsole(locals=globals())
    console.interact()


def osc():
    global run_loop
    while run_loop:
        oscserver.recv(1)


replThread = threading.Thread(target=repl)
oscThread = threading.Thread(target=osc)


def parse_arguments():
    global hostname, tracker_links, repl_active
    if not len(sys.argv):
        return

    parser = argparse.ArgumentParser()
    parser.add_argument("--repl", help="active the REPL", action='store_true')
    args = parser.parse_args()

    repl_active = args.repl


def splash_init():
    global oscserver
    parse_arguments()
    try:
        oscserver = OSCServer('localhost', 9000)
        print("OSC server running at: ", oscserver.getPort())
        oscserver.recv(0)
    except Exception as e:
        oscserver = None
        print("could not start OSC server - ", e)

    if repl_active:
        replThread.start()

    oscThread.start()


def splash_loop():
    global eye_position
    new_eye_position = oscserver.eye_position

    if new_eye_position != eye_position:
        eye_position = new_eye_position
        splash.set_object_attribute("texCoordGenerator", "eyeOrientation", [0.0, 1.0, 0.0])
        splash.set_object_attribute("texCoordGenerator", "method", 'Equirectangular projection')
        splash.set_object_attribute("texCoordGenerator", "eyePosition", eye_position)
        splash.set_object_attribute("texCoordGenerator", "meshName", "mesh")
        splash.set_object_attribute("texCoordGenerator", "generate", True)

    return True


def splash_stop():
    global run_loop
    run_loop = False
    if repl_active:
        print("Press a key to quit")
        console.push("quit()")
