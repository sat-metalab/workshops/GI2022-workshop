/// Copy to build dir and version only static media used in slides

var nodegit = require("nodegit");
const path = require("path");
const fs = require("fs-extra");
const dest = "build/";

var repo = null;
var index = null;

/// https://stackoverflow.com/questions/1960473/get-all-unique-values-in-a-javascript-array-remove-duplicates
function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

/// https://github.com/webpro/reveal-md#pre-process-markdown
module.exports = (markdown, options) => {
  console.log("static media preprocessor");
  return new Promise((resolve, reject) => {
    console.log("static media preprocessor");
    media = [];
    markdown.split("\n").map((line, index) => {
      const regex = /(images|sounds|videos|web)\/[^"]*/g;
      const found = line.match(regex);
      if (found !== null) {
        media = media.concat(found[0]);
        // console.log(found, index, line);
      }
    });
    media = media.filter(onlyUnique).sort();
    // console.log("media", media);

    /// Copy to build dir only static media used in slides
    media.forEach((filepath) => {
      filepath = decodeURI(filepath)
      console.log("Copying", filepath, "to", dest);
      fs.copy(path.join(filepath), path.join(dest, filepath))
        // .then((result) => console.log('success!',result))
        .catch((err) => console.error(err));
    });

    /// Version only static media used in slides
    nodegit.Repository.open(".")
      .then(function (_repo) {
        repo = _repo;
        console.log("Using " + repo.path());
        console.log(repo);
        return repo.refreshIndex();
      })
      .then(function (_index) {
        index = _index;
        console.log(index);
        return index
          .addAll(media)
          .then(function (result) {
            console.log("Added media files to git repository");
            return index.write();
          })
          .then(function (result) {
            console.log("Updated git index");
            return;
          });
      });
    console.log("Returning markdown");
    return resolve(markdown);
  });
};
