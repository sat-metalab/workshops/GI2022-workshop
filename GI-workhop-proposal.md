# Creating Spontateous and Mobile Immersive Spaces using FLOSS


## Topic

This workshop is aimed at artists, designers, content creators and other creatives who are interested in creating immersive spaces using low-cost and lightweight equipment. 
We will be using off-the-shelf hardware and open-source software to create interactive and immersive experiences, from scratch, in impromptu spaces. 
We will present the tools and the process for deploying immersive experiences: characterizing the available space, setting up projection mapping, calibrating video projectors and sound equipment, setting up cameras for real-time pose tracking and estimation, and employing a software pipeline to glue all components together.

The workshop is based on using free/libre open-source software as much as possible, democratizing what's possible to do with computer vision, computer graphics and spatialized audio technologies that are still marginalized and on the fringes of creative activities.

We will be exploring production pipelines that illustrate the use of software developed by the Metalab in tandem with existing tools that have been seeing wider adoption in recent years. For clarity, we present the proposed toolset in two groups: software developed by the Metalab and third-party software.

-   Metalab software:
    -   LivePose - for interpreting live pose estimation using machine learning
    -   Splash - for video mapping
    -   SATIE - for sound rendering and spatialization
-   Other possible software:
    -   Godot Game Engine - live 3D renderer for interactive content
    -   Chataigne - show control and "glue" for data mapping
    -   Ossia Score - show control and "glue" for data mapping

The above software can run on low-cost, low-energy hardware such as Raspberry Pi and NVIDIA Jetson single board computers. We believe that putting such hardware and open-source software into creative hands can bring awe and wonder as much, if not more, than the technological bloat that we experience today.

The call for participation to this workshop will emphasize first and foremost the collaboration, sharing and accessibility aspects from where an art piece can emerge from. Collaboration and sharing aspects are brought firstly by the creation of a single interactive and immersive experience, all participants being encouraged to come with ideas and content and to be open to the suggestions of other participants.

Sharing and accessibility are brought by the use of technologies which are easy to obtain. In addition to cameras, videoprojectors and audio speakers, the workshop makes use of free software as well as low-cost hardware. Some of the software is already well known in the community (Godot, Chataigne), whereas our own software is not as much but it addresses use cases which are either not possible with other software or involve costly solutions.


## Plan of the workshop

-   Introduction to the challenges of creating immersive artworks
-   Summary overview of different solutions (software/hardware) to address the challenges
-   Introduction to the Metalab software/hardware ecosystem
-   Implementation of a simple immersive (audio-visual and interactive) installation with attendees participation
-   Discussion


## Requirements

-   a suitably sized room for the number of participants with controlled lighting (and away from windows). Ideally a dedicated corner of room to demonstrate setting up a video mapping onto non-planar surfaces
-   controlled lighting or the ability to dim or switch off the lights to demonstrate video projections
-   freedom to play sounds without disturbing others

## Bios

Founded in 2002, Metalab is the research laboratory of the Society for Arts and Technology [SAT]. Metalab's mission is twofold: to stimulate the emergence of innovative immersive experiences and to make their design accessible to artists and creators of immersion through an ecosystem of free software that addresses problems that are not easily solved by existing tools. The software (and recently hardware) developed at the Metalab is often used with other existing technologies to facilitate the mixing of video mapping, 3D audio, telepresence and interaction.

**Emmanuel Durand** holds a doctorate in computer graphics from Arts et Métiers ParisTech (2013).
He has an interest in everything related to 3D, photography and space.

**Christian Frisson** was a doctoral researcher at the University of Mons (numediart Institute, ISIA Lab) and a postdoctoral researcher at Inria (Mjolnir team), at the University of Calgary (Interactions Lab), and at McGill University (IDMIL).
He creates tools to author interactive multimedia materials; using computer vision, computer graphics, and haptic and sound feedback.

**Michał Seta** is a composer, improviser and researcher in digital arts.
Practitioner of transdisciplinary, transcalar, and integrative magic, he incants Metalab software in collective and improvised harmony.
