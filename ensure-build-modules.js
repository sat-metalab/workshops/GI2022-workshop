const fs = require('fs-extra')
const dir = 'build/module'

fs.ensureDir(dir)
.then(() => {
  console.log(dir+' exists')
})
.catch(err => {
  console.error(err)
})