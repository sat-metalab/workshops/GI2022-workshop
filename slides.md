---
title: "Creating Spontaneous and Mobile Immersive Spaces using FLOSS | Graphics Interface 2022 Workshop"
theme: white
separator: <!--section-->
verticalSeparator: <!--slide-->

revealOptions:
  transition: "none"
  slideNumber: "c/t"
  hashOneBasedIndex: true
  history: true
  margin: 0.01
  controls: true
  progress: true
  dependencies:
    - src: "module/reveal.js-plugins/chart/Chart.min.js"
    - src: "module/reveal.js-plugins/chart/csv2chart.js"
    - src: "module/reveald3/reveald3.js"
    - src: "module/reveal.js-plugins/spreadsheet/ruleJS.all.full.min.js"
    - src: "module/reveal.js-plugins/spreadsheet/spreadsheet.js"
    - src: "module/reveal-a11y/accessibility/helper.js"
---

<!-- Use this to add a background image: -->
<!-- .slide: class="" data-background="images/mantissa.xyz_loop_014_GI22.png" id="start" data-state="start"-->

<!-- Use this transparent styling in case of a background image: -->
<div class="title">

<h2>Creating Spontaneous and Mobile <br/>Immersive Spaces using FLOSS</h2>

### Graphics Interface 2022 Workshop

<!-- <div class="affiliations"> -->
<!-- <div class="affiliation"> -->
<div class="event">
<a class="logo" href="https://graphicsinterface.org/conference/2022/" title="Graphics Interface 2022"><img src="images/logos/gi-logo.png" alt="Graphics Interface 2022"/></a>
</div>
<!-- </div> -->

<br/>

<!-- </div>  -->

<div class="authors">

<div class="author">
<img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
<div class="name">
<a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
</div>
</div>

<div class="author">
<img src="images/portraits/MichalSeta.jpg" alt="Michał Seta"/>
<div class="name">
<a href="mailto:mseta@sat.qc.ca" title="Michał Seta's Email">Michał</a></span> <a href="http://djiamnot.xyz" title="Michał Seta's Website">Seta</a></span>
</div>
</div>

<div class="author">
<img src="images/portraits/EmmanuelDurand.png" alt="Emmanuel Durand"/>
<div class="name">
<a href="mailto:edurand@sat.qc.ca" title="Emmanuel Durand's Email">Emmanuel</a></span> <a href="https://emmanueldurand.net" title="Emmanuel Durand's Website">Durand</a></span>
</div>
</div>

</div>

<br/>

<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/SAT.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

#### 2022-05-16

</div class="title">

<!--section-->

<div class="title">

<h2>Creating Spontaneous and Mobile <br/>Immersive Spaces using FLOSS</h2>

### Graphics Interface 2022 Workshop

<!-- <div class="affiliations"> -->
<!-- <div class="affiliation"> -->
<div class="event">
<a class="logo" href="https://graphicsinterface.org/conference/2022/" title="Graphics Interface 2022"><img src="images/logos/gi-logo.png" alt="Graphics Interface 2022"/></a>
</div>
<!-- </div> -->

<br/>

<!-- </div>  -->

<div class="authors">

<div class="author">
<img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
<div class="name">
<a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
</div>
</div>

<div class="author">
<img src="images/portraits/MichalSeta.jpg" alt="Michał Seta"/>
<div class="name">
<a href="mailto:mseta@sat.qc.ca" title="Michał Seta's Email">Michał</a></span> <a href="http://djiamnot.xyz" title="Michał Seta's Website">Seta</a></span>
</div>
</div>

<div class="author">
<img src="images/portraits/EmmanuelDurand.png" alt="Emmanuel Durand"/>
<div class="name">
<a href="mailto:edurand@sat.qc.ca" title="Emmanuel Durand's Email">Emmanuel</a></span> <a href="https://emmanueldurand.net" title="Emmanuel Durand's Website">Durand</a></span>
</div>
</div>

</div>

<br/>

<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/SAT.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

#### 2022-05-16

</div class="title">

<!--slide-->

<!-- .slide: class="" data-background="images/3rdparty/Habitat67/Habitat-67-montreal-vers-lest-JB-2019-goodzise.jpeg" id="start" data-state="start"-->

<!-- https://www.habitat67.com -->

Notes:
* Do you recognize this city?
* Can you guess where this picture has been taken from?

<!--slide-->

<!-- .slide: class="" data-background="images/3rdparty/Habitat67/Habitat_67_Montreal.jpg" id="start" data-state="start"-->

<!-- https://en.wikipedia.org/wiki/Habitat_67 -->

Notes:
* Let's use another viewpoint
* Intermission: by the way, have some of you played video game Myst?
* Any small detail you can spot in the picture?

<!--slide-->

<!-- .slide: class="" data-background="images/3rdparty/Biosphere/1920px-17-08-islcanus-RalfR-DSC_3883.jpg" id="start" data-state="start"-->

<!-- https://en.wikipedia.org/wiki/Montreal_Biosphere -->

Notes:
* This is the small detail: Montreal's Biosphere.

<!--slide-->

<!-- .slide: class="" data-background="images/3rdparty/Habitat67/H67_juillet_-064-Cramer-Miau-169-goodzise.jpeg" id="start" data-state="start"-->

<!-- https://www.habitat67.com -->

Notes:
* What you see here is Habitat 67 built at the same time as Montreal's Biosphere: for the Expo 1967.
* Looks like well maintained lawn.

<!--slide-->

<!-- .slide: class="" data-background="images/3rdparty/Habitat67/Habitat_panorama.jpg" id="start" data-state="start"-->

<!-- https://www.habitat67.com -->

Notes:
* Here's is another perspective, less appealing. 
* A glimpse into the future? 
* Imagine overpopulation, climate change, ... 100 years after.
* How would Expo 2067 look?

<!--slide-->

<!-- .slide: class="" data-background="images/mantissa.xyz_loop_014_GI22.png" id="start" data-state="start"-->

Notes:
* Could this be Expo 2067?
* Let's envision it by creating a spontaneous and mobile immersive space using FLOSS.

<!--slide-->

## Outline


### Introduction

### Tools

#### Presentation
#### Construction

### Discussion

<!--section-->

## Open-Source Tools

<div class="row">

<div class="col-33 fragment" data-fragment-index="1">

### Projection Mapping 

</div>

<div class="col-33 fragment" data-fragment-index="2">

### Sound Spatialization 

</div>

<div class="col-33 fragment" data-fragment-index="3">

### Camera-based Interaction

</div>

</div>

<div class="row">
<div class="col-33 fragment tool" data-fragment-index="1">

<img  src="images/logos/Splash_RGB.png"/>


</div>

<div class="col-33 fragment tool" data-fragment-index="2">

<img  src="images/logos/Satie_RGB.png"/>

</div>
<div class="col-33 fragment tool" data-fragment-index="3">

<img  src="images/logos/Logo-LivePose---Vertical-couleur.png"/>

</div>
</div>

<div class="row">

<div class="col-33 fragment" data-fragment-index="1">

### [Splash](https://gitlab.com/sat-metalab/splash) 

</div>

<div class="col-33 fragment" data-fragment-index="2">

### [SATIE](https://gitlab.com/sat-metalab/satie) 

</div>

<div class="col-33 fragment" data-fragment-index="3">

### [LivePose](https://gitlab.com/sat-metalab/livepose)

</div>

</div>

<!--section-->
## Open-Source Tools

<div class="row">

<div class="col-33" style="opacity: 1">

### Projection Mapping 

</div>

<div class="col-33" style="opacity: 0.1">

### Sound Spatialization 

</div>

<div class="col-33" style="opacity: 0.1">

### Camera-based Interaction

</div>

</div>

<div class="row">
<div class="col-33 tool" style="opacity: 1">

<img  src="images/logos/Splash_RGB.png"/>


</div>

<div class="col-33 tool" style="opacity: 0.1">

<img  src="images/logos/Satie_RGB.png"/>

</div>
<div class="col-33 tool" style="opacity: 0.1">

<img  src="images/logos/Logo-LivePose---Vertical-couleur.png"/>

</div>
</div>

<div class="row">

<div class="col-33" style="opacity: 1">

### [Splash](https://gitlab.com/sat-metalab/splash) 

</div>

<div class="col-33" style="opacity: 0.1">

### [SATIE](https://gitlab.com/sat-metalab/satie) 

</div>

<div class="col-33" style="opacity: 0.1">

### [LivePose](https://gitlab.com/sat-metalab/livepose)

</div>

</div>


<!--slide-->

# Projection mapping with Splash

Projection mapping

[https://sat-metalab.gitlab.io/splash](https://sat-metalab.gitlab.io/splash)

<!--slide-->

# Projection mapping with Splash

"Projection Mapping uses everyday video projectors, but instead of projecting on a flat screen (...), light is mapped onto any surface, turning common objects of any 3D shape into interactive displays."

-- <cite>[http://projection-mapping.org/what-is-projection-mapping/](http://projection-mapping.org/what-is-projection-mapping/)</cite>

<!--slide-->

## Projection onto abritrary geometries

![](images/realObjectWithVideo.jpg)

<!--slide-->

## Projection onto abritrary geometries

![](images/posture-interaction.png)


<!--slide-->

## Projection onto mobile objects

<iframe src="https://player.vimeo.com/video/268028595" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--slide-->

## Aligning projection onto the physical objects

![](images/realObjectWireframe.jpg)

<!--slide-->

## Handling projection overlaps

![](images/semidome_wireframe_no_blending.jpg)

<!--slide-->

## Hands on!

Calibrating a diorama with Splash

<!--section-->


## Open-Source Tools

<div class="row">

<div class="col-33" style="opacity: 0.1">

### Projection Mapping 

</div>

<div class="col-33" style="opacity: 1">

### Sound Spatialization 

</div>

<div class="col-33" style="opacity: 0.1">

### Camera-based Interaction

</div>

</div>

<div class="row">
<div class="col-33 tool" style="opacity: 0.1">

<img  src="images/logos/Splash_RGB.png"/>


</div>

<div class="col-33 tool" style="opacity: 1">

<img  src="images/logos/Satie_RGB.png"/>

</div>
<div class="col-33 tool" style="opacity: 0.1">

<img  src="images/logos/Logo-LivePose---Vertical-couleur.png"/>

</div>
</div>

<div class="row">

<div class="col-33" style="opacity: 0.1">

### [Splash](https://gitlab.com/sat-metalab/splash) 

</div>

<div class="col-33" style="opacity: 1">

### [SATIE](https://gitlab.com/sat-metalab/satie) 

</div>

<div class="col-33" style="opacity: 0.1">

### [LivePose](https://gitlab.com/sat-metalab/livepose)

</div>

</div>

<!--slide-->

# Audio spatialization with SATIE

Spatial Audio Toolkit for Immersive Environments

[https://sat-metalab.gitlab.io/satie](https://sat-metalab.gitlab.io/satie)

<!--slide-->

## Audio spatialization with SATIE

* synchronized with 3D engines
* capable of handling a lot of audio speakers
* multiple simultaneous audio rendering
* many audio sources

<!--slide-->

## SATIE - why?

audio object

<iframe src="https://player.vimeo.com/video/398346954?h=d4ab3f4b2a#t=0m37s&autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--slide-->

## SATIE is for:

* audio sources:
  * live inputs
  * audio synthesis
* effects

<!--slide-->

## SATIE - how?

* OSC (Open Sound Control)
* Applications:
  * Godot Game Engine
  * Unity3D
  * Blender
  * Max(4Live), Pure Data, etc...

<!--slide-->

## Demo

Here are some examples of SATIE in action:

<iframe src="https://player.vimeo.com/video/616508216" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>


<!--section-->

## Open-Source Tools

<div class="row">

<div class="col-33" style="opacity: 0.1">

### Projection Mapping 

</div>

<div class="col-33" style="opacity: 0.1">

### Sound Spatialization 

</div>

<div class="col-33" style="opacity: 1">

### Camera-based Interaction

</div>

</div>

<div class="row">
<div class="col-33 tool" style="opacity: 0.1">

<img  src="images/logos/Splash_RGB.png"/>


</div>

<div class="col-33 tool" style="opacity: 0.1">

<img  src="images/logos/Satie_RGB.png"/>

</div>
<div class="col-33 tool" style="opacity: 1">

<img  src="images/logos/Logo-LivePose---Vertical-couleur.png"/>

</div>
</div>

<div class="row">

<div class="col-33" style="opacity: 0.1">

### [Splash](https://gitlab.com/sat-metalab/splash) 

</div>

<div class="col-33" style="opacity: 0.1">

### [SATIE](https://gitlab.com/sat-metalab/satie) 

</div>

<div class="col-33" style="opacity: 1">

### [LivePose](https://gitlab.com/sat-metalab/livepose)

</div>

</div>


<!--slide-->

# LivePose

Real-time Pose Estimation for Video Streams

[https://gitlab.com/sat-metalab/livepose](https://gitlab.com/sat-metalab/livepose)

<!--slide-->

## Pose Estimation

**Goal**: To estimate the pose of a person in an image by locating special body
points (**keypoints**).

![](images/livepose/keypoints_and_people.png)

<!--slide-->

## In The Satosphere
<iframe src="images/livepose/openpose_walk_0.mp4" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--slide-->

## LivePose Features

**Accessibility**: Easy to use in many different settings.

<!--slide-->

## LivePose Features

**Accessibility**: Easy to use in many different settings.

* Easy to install on Ubuntu 20.04! Ready to run out-of-the-box.

<!--slide-->

## LivePose Features

Multiple **output** formats available.

* **OSC** (Open Sound Control)
* Use OSC messages to update sound and lighting in an interactive environment based on poses

<!--slide-->

## LivePose Features

Multiple **output** formats available.

* **Websocket**: For controlling web applications with real-time pose data.

<iframe src="https://player.vimeo.com/video/604196712?h=d4ab3f4b2a#&autoplay=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

<!--slide-->

## LivePose Features

Multiple Pose Estimation models supported for different use cases.

* **PoseNet**: lightweight and fast, can run on mobile and embedded devices
* **TRTPose**: NVIDIA accelerated
* **MMPose**: library of various 2D and 3D models

<!--slide-->

## LivePose Features

Runs on embedded hardware
Specific support for NVIDIA Jetson boards

<!--slide-->

## LivePose Features

* **Action** and **Gesture** Detection
* Control an interactive environment with gestures like raising arms, jumping, etc.

![](images/livepose/arms_up.png)

<!--slide-->

## Hands on!

Using LivePose to adapt Splash projection

<!--section-->

# Discussion

<!--slide-->

# Use cases and gotchas

* Content depends on the imersive space
* Interaction metaphors depend on the immersive space and the public
* Multiple users possible, but a single optimal point of view

<!--section-->


<!-- Use this to add a background image: -->
<!-- .slide: class="" data-background="images/mantissa.xyz_loop_014_GI22.png" id="end" data-state="end"-->

<!-- Use this transparent styling in case of a background image: -->
<div class="title">

<h2>Creating Spontaneous and Mobile <br/>Immersive Spaces using FLOSS</h2>

### Graphics Interface 2022 Workshop

<!-- <div class="affiliations"> -->
<!-- <div class="affiliation"> -->
<div class="event">
<a class="logo" href="https://graphicsinterface.org/conference/2022/" title="Graphics Interface 2022"><img src="images/logos/gi-logo.png" alt="Graphics Interface 2022"/></a>
</div>
<!-- </div> -->

<br/>

<!-- </div>  -->

<div class="authors">

<div class="author">
<img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
<div class="name">
<a href="mailto:christian@frisson.re" title="Christian Frisson's Email">Christian</a></span> <a href="https://frisson.re" title="Christian Frisson's Website">Frisson</a></span>
</div>
</div>

<div class="author">
<img src="images/portraits/MichalSeta.jpg" alt="Michał Seta"/>
<div class="name">
<a href="mailto:mseta@sat.qc.ca" title="Michał Seta's Email">Michał</a></span> <a href="http://djiamnot.xyz" title="Michał Seta's Website">Seta</a></span>
</div>
</div>

<div class="author">
<img src="images/portraits/EmmanuelDurand.png" alt="Emmanuel Durand"/>
<div class="name">
<a href="mailto:edurand@sat.qc.ca" title="Emmanuel Durand's Email">Emmanuel</a></span> <a href="https://emmanueldurand.net" title="Emmanuel Durand's Website">Durand</a></span>
</div>
</div>

</div>

<br/>

<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/SAT.png" alt="Société des Arts Technologiques"/></a>
<a class="logo" href="https://sat-metalab.gitlab.io" title="Metalab"><img src="images/logos/Metalab_Logo_Noir.png" alt="Metalab"/></a>
</div>
</div>

#### 2022-05-16

</div class="title">
